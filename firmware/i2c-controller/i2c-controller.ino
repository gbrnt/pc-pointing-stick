/*
 *  I2C controller for pointing stick
 *  This is an example of what you would do on the keyboard microcontroller
 */

#include <Wire.h>

#define STICK_I2C_ADDRESS 0x22

// This must be the same as the one on the peripheral
enum i2c_commands {
  MODE_SELECT,
  //RUN_PAUSE,
  //STICK_RECENTRE,
  STICK_DEADZONE_RADIUS,
  STICK_INVERT_X,
  STICK_INVERT_Y,
  STICK_BASELINE_X,
  STICK_BASELINE_Y,
  STICK_SPEED,
  STICK_RAW_X,
  STICK_RAW_Y,
  STICK_SCALED_X,
  STICK_SCALED_Y,
  //POT_AUTO_CENTRE_EN,
  //POT_RECENTRE,
  POT_CENTRE_X,
  POT_CENTRE_Y,
  LED_1,
  LED_2,
};


byte invert_x = false, invert_y = false;
byte led_1 = false, led_2 = false;


void setup() {
  Serial.begin(115200);
  Wire.begin();  // Join i2c bus as controller
}


void loop() {
  byte buf[2]; // Buffer for i2c values

  while (Serial.available()) {
    switch(Serial.read()) {
    case 'x':
      // Toggle stick_invert_x setting
      invert_x = !invert_x;
      stickWrite(STICK_INVERT_X, 1, &invert_x);
      break;

    case 'y':
      // Toggle stick_invert_x setting
      invert_y = !invert_y;
      stickWrite(STICK_INVERT_Y, 1, &invert_y);
      break;

    case 'X':
      // Read raw X analog value
      stickRead(STICK_RAW_X, 2, buf);
      Serial.println((buf[0] << 8) + buf[1]);
      break;

    case 'Y':
      // Read raw Y analog value
      stickRead(STICK_RAW_Y, 2, buf);
      Serial.println((buf[0] << 8) + buf[1]);
      break;

    case '1':
      // Toggle LED 1 and read it
      led_1 = !led_1;
      stickWrite(LED_1, 1, &led_1);
      delay(2);
      stickRead(LED_1, 1, buf);
      Serial.println(buf[0]);
      break;

    case '2':
      // Toggle LED 2 and read it
      led_2 = !led_2;
      stickWrite(LED_2, 1, &led_2);
      delay(2);
      stickRead(LED_2, 1, buf);
      Serial.println(buf[0]);
      break;

    case 'u':
      // Set to USB mode
      buf[0] = 0;
      stickWrite(MODE_SELECT, 1, buf);
      stickRead(MODE_SELECT, 1, buf);
      Serial.println(buf[0]);
      break;

    case 'U':
      // Set to i2c auto mode
      buf[0] = 1;
      stickWrite(MODE_SELECT, 1, buf);
      delay(2);
      stickRead(MODE_SELECT, 1, buf);
      Serial.println(buf[0]);
      break;

    default:
      break;
    }
  }
}


void stickWrite(uint8_t command, uint8_t length, byte * data) {
  /*
   *  Write a register on the pointing stick over i2c
   */

  Wire.beginTransmission(STICK_I2C_ADDRESS);
  Wire.write(command);
  for (uint8_t i=0; i<length; i++) {
    Wire.write(data[i]);
  }
  Wire.endTransmission();
}


void stickRead(uint8_t command, uint8_t response_length, byte * buf) {
  /*
   *  Read a register from the pointing stick over i2c
   *  buf needs to be big enough for the response
   *  Otherwise unwanted stuff can be overwritten
   */

  // First write command so stick knows what to send back
  Wire.beginTransmission(STICK_I2C_ADDRESS);
  Wire.write(command);
  Wire.endTransmission();

  // Now request and read the data back
  Wire.requestFrom(STICK_I2C_ADDRESS, response_length);
  for (uint8_t i=0; i<response_length; i++) buf[i] = Wire.read();
}
