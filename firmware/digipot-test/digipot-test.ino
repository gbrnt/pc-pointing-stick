/*
 *  USB/i2c pointing stick
 *  Digital potentiometer test program
 */

// Output pins
#define PIN_VIN_X A0 // PF7 = analog 0
#define PIN_VIN_Y A1 // PF6 = analog 1
#define LED_USER1 13 // PC7 = digital 13
#define LED_USER2 5 // PC6 = digital 5

#include "MCP4251.h"

uint8_t centre_x = 127, centre_y = 127;


void setup() {
  Serial.begin(115200);

  pinMode(LED_USER1, OUTPUT);
  pinMode(LED_USER2, OUTPUT);
  digitalWrite(LED_USER1, HIGH); // High = off
  digitalWrite(LED_USER2, HIGH); // High = off

  MCP4251_Init();

  // Flash both user LEDs to show startup
  digitalWrite(LED_USER1, LOW);
  digitalWrite(LED_USER2, LOW);
  delay(250);
  digitalWrite(LED_USER1, HIGH);
  digitalWrite(LED_USER2, HIGH);
}


void loop() {
  // Loop through all positions on digital potentiometer
  for (int i=0; i<256; i++) {
    MCP4251_Write(ADDR_POT_WIPER0_W, i);
    MCP4251_Write(ADDR_POT_WIPER1_W, i);
    delay(25);
  }
}
