/*
 *  Digital potentiometer control library
 *  Based on https://matthewcmcmillan.blogspot.com/2014/03/arduino-using-digital-potentiometers.html
 */


#include <SPI.h>

// Pins for controlling the pot
// SPI pins are predefined on the ATMega32u4
#define PIN_POT_CS A5 // PF0 = analog 5
#define PIN_POT_SHDN A4 // PF1 = analog 4


// Handy axis enumeration
enum AXES {AXIS_X=0, AXIS_Y=1};


// MCP4251 commands
// Send two bytes, one with the memory address and one with the value to set at that address
// The MCP4251 has a few for wipers and terminal control (TCON)
// TCON allows connecting and disconnecting the terminals to save power
#define ADDR_POT_WIPER0_W B00000000
#define ADDR_POT_WIPER1_W B00010000
#define ADDR_POT_TCON_W   B01000000


// Function prototypes
void MCP4251_Init(void);
void MCP4251_Write(uint8_t address, uint8_t value);
void MCP4251_Set(uint8_t axis, uint8_t value);


void MCP4251_Init(void) {
  // Initialise SPI and turn off pot shutdown
  pinMode(PIN_POT_CS, OUTPUT);
  pinMode(PIN_POT_SHDN, OUTPUT);
  digitalWrite(PIN_POT_CS, HIGH);   // Active low - deselect
  digitalWrite(PIN_POT_SHDN, HIGH); // Shutdown is active low

  SPI.begin();

  // Set wiper to centre
  MCP4251_Write(ADDR_POT_WIPER0_W, 127);
  MCP4251_Write(ADDR_POT_WIPER1_W, 127);
}


void MCP4251_Write(uint8_t address, uint8_t value) {
  /*
   *  Write SPI data to the potentiometer
   */
  // Bring the chip select pin low to select the pot
  digitalWrite(PIN_POT_CS, LOW);
  // Send the address and value via SPI
  SPI.transfer(address);
  SPI.transfer(value);
  digitalWrite(PIN_POT_CS, HIGH);
}


void MCP4251_Set(uint8_t axis, uint8_t position) {
  /*
   *  Set one axis of the digital potentiometer to a certain position
   */

  uint8_t addr;

  if (axis == AXIS_X) addr = ADDR_POT_WIPER0_W;
  else if (axis == AXIS_Y) addr = ADDR_POT_WIPER1_W;

  MCP4251_Write(addr, position);
}
