// 7-bit address 0x22 doesn't seem to be used much
#define I2C_ADDRESS 0x22

// This must be the same as the one on the controller
enum i2c_commands {
  MODE_SELECT,
  //RUN_PAUSE,
  //STICK_RECENTRE,
  STICK_DEADZONE_RADIUS,
  STICK_INVERT_X,
  STICK_INVERT_Y,
  STICK_BASELINE_X,
  STICK_BASELINE_Y,
  STICK_SPEED,
  STICK_RAW_X,
  STICK_RAW_Y,
  STICK_SCALED_X,
  STICK_SCALED_Y,
  //POT_AUTO_CENTRE_EN,
  //POT_RECENTRE,
  POT_CENTRE_X,
  POT_CENTRE_Y,
  LED_1,
  LED_2,
};


static byte last_i2c_command;


void handleI2CData(int num_bytes) {
  /*
   *  Controller is sending data
   *  If num_bytes is 1, assume it's just setting the address for a read
   */

  byte buf[2]; // Buffer for 16-bit i2c values

  // First byte will be the command
  byte command = Wire.read();

  // Save command for future reads
  last_i2c_command = command;

  // If that was all that was sent, there's no more data to handle - return early
  // This was likely to set the command variable for a read
  if (num_bytes == 1) return;

  switch(command) {
  case MODE_SELECT:
    registers.operation_mode = Wire.read();
    break;
  //case RUN_PAUSE:
  //case STICK_RECENTRE:
  case STICK_DEADZONE_RADIUS:
    registers.stick_deadzone_radius = Wire.read();
    break;
  case STICK_INVERT_X:
    registers.stick_invert_x = Wire.read();
    break;
  case STICK_INVERT_Y:
    registers.stick_invert_y = Wire.read();
    break;
  case STICK_BASELINE_X:
    buf[0] = Wire.read();
    buf[1] = Wire.read();
    registers.stick_baseline_x = (buf[0]<<8) + buf[1];
    break;
  case STICK_BASELINE_Y:
    buf[0] = Wire.read();
    buf[1] = Wire.read();
    registers.stick_baseline_y = (buf[0]<<8) + buf[1];
    break;
  case STICK_SPEED:
    registers.stick_speed = Wire.read();
    break;
  case STICK_RAW_X:
    // No point writing this as it will be overwritten
    buf[0] = Wire.read();
    buf[1] = Wire.read();
    registers.stick_raw_x = (buf[0]<<8) + buf[1];
    break;
  case STICK_RAW_Y:
    // No point writing this as it will be overwritten
    buf[0] = Wire.read();
    buf[1] = Wire.read();
    registers.stick_raw_y = (buf[0]<<8) + buf[1];
    break;
  case STICK_SCALED_X:
    // No point writing this as it will be overwritten
    buf[0] = Wire.read();
    buf[1] = Wire.read();
    registers.stick_scaled_x = (buf[0]<<8) + buf[1];
    break;
  case STICK_SCALED_Y:
    // No point writing this as it will be overwritten
    buf[0] = Wire.read();
    buf[1] = Wire.read();
    registers.stick_scaled_y = (buf[0]<<8) + buf[1];
    break;
  //case POT_AUTO_CENTRE_EN:
  //case POT_RECENTRE:
  case POT_CENTRE_X:
    registers.pot_centre_x = Wire.read();
    MCP4251_Set(AXIS_X, registers.pot_centre_x);
    break;
  case POT_CENTRE_Y:
    registers.pot_centre_y = Wire.read();
    MCP4251_Set(AXIS_Y, registers.pot_centre_y);
    break;
  case LED_1:
    // Invert value so that 1 is LED on, 0 is LED off
    // Set register and it will be updated in the next main loop
    registers.led_1 = !Wire.read();
    break;
  case LED_2:
    // Invert value so that 1 is LED on, 0 is LED off
    // Set register and it will be updated in the next main loop
    registers.led_2 = !Wire.read();
    break;
  default:
    Serial.println("?command?");
    break;
  }
}


void handleI2CRequest() {
  /*
   *  Controller (e.g. attached keyboard) has requested data from the pointing stick
   *  Use last i2c command to decide what to send back
   */

  switch(last_i2c_command) {
  case MODE_SELECT:
    Wire.write(registers.operation_mode);
    break;
  //case RUN_PAUSE:
  //case STICK_RECENTRE:
  case STICK_DEADZONE_RADIUS:
    Wire.write(registers.stick_deadzone_radius);
    break;
  case STICK_INVERT_X:
    Wire.write(registers.stick_invert_x);
    break;
  case STICK_INVERT_Y:
    Wire.write(registers.stick_invert_y);
    break;
  case STICK_BASELINE_X:
    Wire.write((registers.stick_baseline_x >> 8) & 0xFF);
    Wire.write(registers.stick_baseline_x & 0xFF);
    break;
  case STICK_BASELINE_Y:
    Wire.write((registers.stick_baseline_y >> 8) & 0xFF);
    Wire.write(registers.stick_baseline_y & 0xFF);
    break;
  case STICK_SPEED:
    Wire.write(registers.stick_speed);
    break;
  case STICK_RAW_X:
    Wire.write((registers.stick_raw_x >> 8) & 0xFF);
    Wire.write(registers.stick_raw_x & 0xFF);
    break;
  case STICK_RAW_Y:
    Wire.write((registers.stick_raw_y >> 8) & 0xFF);
    Wire.write(registers.stick_raw_y & 0xFF);
    break;
  case STICK_SCALED_X:
    Wire.write((registers.stick_scaled_x >> 8) & 0xFF);
    Wire.write(registers.stick_scaled_x & 0xFF);
    break;
  case STICK_SCALED_Y:
    Wire.write((registers.stick_scaled_y >> 8) & 0xFF);
    Wire.write(registers.stick_scaled_y & 0xFF);
    break;
  //case POT_AUTO_CENTRE_EN:
  //case POT_RECENTRE:
  case POT_CENTRE_X:
    Wire.write(registers.pot_centre_x);
    break;
  case POT_CENTRE_Y:
    Wire.write(registers.pot_centre_y);
    break;
  case LED_1:
    // Invert value so that 1 is LED on, 0 is LED off
    Wire.write(!registers.led_1);
    break;
  case LED_2:
    // Invert value so that 1 is LED on, 0 is LED off
    Wire.write(!registers.led_2);
    break;
  default:
    Serial.println("?command?");
    break;
  }
}
