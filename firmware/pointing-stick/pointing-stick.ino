/*
 *  USB/i2c pointing stick
 */


// Output pins
//#define POT_CS A5 // PF0 = analog 5
#define LED_USER1 13 // PC7 = digital 13
#define LED_USER2 5 // PC6 = digital 5
#define PIN_MODE 7 // PE6 = digital 7
//#define AXIS_SWAP

// Swap X and Y axes by defining AXIS_SWAP before this
#ifndef AXIS_SWAP
#define PIN_VIN_X A0 // PF7 = analog 0
#define PIN_VIN_Y A1 // PF6 = analog 1
#else
#define PIN_VIN_X A1 // PF6 = analog 1
#define PIN_VIN_Y A0 // PF7 = analog 0
#endif

// LED states - states are inverted from typical bool
#define LED_ON 0
#define LED_OFF 1

#include <TimerOne.h>
#include "MCP4251.h"
#include <Mouse.h>
#include "registers.h"
#include <Wire.h>
#include "i2c_interface.h"

uint8_t centre_count = 0;

void setup() {
  Serial.begin(115200);

  pinMode(LED_USER1, OUTPUT);
  pinMode(LED_USER2, OUTPUT);
  digitalWrite(LED_USER1, HIGH); // High = off
  digitalWrite(LED_USER2, HIGH); // High = off
  pinMode(PIN_MODE, INPUT_PULLUP); // Pulled up by default

  // Set up registers with initial state
  initRegisters();

  // Initialise digital potentiometer
  MCP4251_Init();
  // Set the centre position to account for offset
  // May take more iterations than this so max limit may need to be increased
  // Exits early if it's acceptably centred
  for (uint8_t i=0; i<10; i++) {
    if (checkIfCentred(AXIS_X) + checkIfCentred(AXIS_Y) != 0) findCentrePos();
    else break;
    delay(1);
  }

  // Join i2c bus
  Wire.begin(I2C_ADDRESS);
  Wire.onReceive(handleI2CData);
  Wire.onRequest(handleI2CRequest);

  // Find centre position for X and Y to use as baseline
  delay(100);
  recalibrate(1000);

  // Start mouse
  Mouse.begin();

  // Set up interrupts to trigger reads
  Timer1.initialize(1000000/registers.stick_read_frequency); // Set timer1 period in microseconds
  Timer1.attachInterrupt(triggerStickRead);

  // Flash both user LEDs to show startup
  setLED(LED_USER1, LED_ON);
  setLED(LED_USER2, LED_ON);
  delay(250);
  setLED(LED_USER1, LED_OFF);
  setLED(LED_USER2, LED_OFF);
}


void loop() {
  // Only update pointer at specific frequency
  if (registers.stick_trigger_read) {
    handlePointer();

    // Re-centre if stick is off
    if (registers.stick_off_centre_count >= registers.stick_recentre_threshold) {
      recalibrate(100);
      registers.stick_off_centre_count = 0;
    }

    registers.stick_trigger_read = false;
  }
  updateLEDState();
}


void findCentrePos() {
  /*
   *  Find position on digital pot that centres the opamp output at VCC/2 (2.5V)
   */

  int8_t change = 0;

  // Centre X axis
  change = checkIfCentred(AXIS_X);
  if (change > 0) registers.pot_centre_x -= 5;
  else if (change < 0) registers.pot_centre_x += 5;

  MCP4251_Set(AXIS_X, registers.pot_centre_x);

  // Centre Y axis
  change = checkIfCentred(AXIS_Y);
  if (change > 0) registers.pot_centre_y -= 5;
  else if (change < 0) registers.pot_centre_y += 5;

  MCP4251_Set(AXIS_Y, registers.pot_centre_y);
}


int8_t checkIfCentred(uint8_t axis) {
  /*
   *  See how well-centred the signal is at the moment
   *  Returns magnitude direction to move in
   */

  int vout, diff;

  if (axis == AXIS_X) {
    vout = analogRead(PIN_VIN_X);    // Read X input voltage
    diff = vout - 512;               // Compare with centre position
    if (diff > 100) return 1;        // If it's off-centre by more than 100, trigger a recentre
    else if (diff < -100) return -1; // Same in the negative direction
    else return 0;                   // Otherwise no need to recentre
  }

  else if (axis == AXIS_Y) {
    vout = analogRead(PIN_VIN_Y);    // Read Y input voltage
    diff = vout - 512;               // Compare with centre position
    if (diff > 100) return 1;        // If it's off-centre by more than 100, trigger a recentre
    else if (diff < -100) return -1; // Same in negative direction
    else return 0;                   // Otherwise no need to recentre
  }
}


float readAverage(uint8_t axis, uint16_t N) {
  /*
   *  Average N readings from sensor
   */

  float average=0;
  uint8_t ax = 0;

  // Select correct pin for the axis
  if (axis == AXIS_X) ax = PIN_VIN_X;
  else if (axis == AXIS_Y) ax = PIN_VIN_Y;

  // Take N readings, gradually building up the average
  // If N is too large it might lose accuracy, not sure how big that would need to be
  for (uint16_t i=0; i<N; i++) {
    average += (float)analogRead(ax) / N;
  }

  return average;
}


void recalibrate(uint16_t num_samples) {
  // Average readings to get a baseline (not sure why there's an offset of 2)
  registers.stick_baseline_x = readAverage(AXIS_X, num_samples) + 2;
  registers.stick_baseline_y = readAverage(AXIS_Y, num_samples) + 2;
}


void handlePointer() {
  /*
   *  Find amount to move pointer and move it
   */

  static float reading_x, reading_y, difference_x, difference_y;

  // Average a bunch of readings of each axis to average out some noise
  reading_x = readAverage(AXIS_X, registers.stick_readings_averaged);
  reading_y = readAverage(AXIS_Y, registers.stick_readings_averaged);

  // Save to registers for reading externally
  registers.stick_raw_x = int(reading_x);
  registers.stick_raw_y = int(reading_y);

  // Find difference between latest reading and baseline
  difference_x = reading_x - registers.stick_baseline_x;
  difference_y = reading_y - registers.stick_baseline_y;

  // Check if close to edge of deadzone
  if (isPointerOffCentre(difference_x, difference_y)) {
    registers.stick_off_centre_count++;
  }
  else {
    registers.stick_off_centre_count = 0;
  }

  // Move pointer only if in USB mode
  if (registers.operation_mode == PS_MODE_USB)
    movePointer(difference_x, -difference_y);  // Invert y axis
}


void movePointer(float difference_x, float difference_y) {
  /*
   *  Do the actual pointer moving
   */

  int16_t input_range = 200; // Radius
  int16_t input_deadzone = registers.stick_deadzone_radius;
  int8_t range = registers.stick_speed;
  int8_t move_x, move_y;

  // Calculate X movement
  if (abs(difference_x) < input_deadzone) {
    // Don't move it if it's not off-centre by enough
    move_x = 0;
  }
  else {
    // Remove deadzone before scaling - we just want how far outside the deadzone you are
    if (difference_x > 0) difference_x -= input_deadzone;
    else difference_x += input_deadzone;

    // Allow axis to be inverted
    if (!registers.stick_invert_x)
      move_x = map((int16_t) difference_x, -input_range, input_range, -range, range);
    else
      move_x = map((int16_t) difference_x, -input_range, input_range, range, -range);
  }

  // Calculate Y movement
  if (abs(difference_y) < input_deadzone) {
    // Don't move it if it's not off-centre by enough
    move_y = 0;
  }
  else {
    // Remove deadzone before scaling - we just want how far outside the deadzone you are
    if (difference_y > 0) difference_y -= input_deadzone;
    else difference_y += input_deadzone;

    // Allow axis to be inverted
    if (!registers.stick_invert_y)
      move_y = map((int16_t) difference_y, -input_range, input_range, -range, range);
    else
      move_y = map((int16_t) difference_y, -input_range, input_range, range, -range);
  }

  // Store in register for external access
  registers.stick_scaled_x = move_x;
  registers.stick_scaled_y = move_y;

  //Serial.print(move_x);
  //Serial.print(",");
  //Serial.println(move_y);
  Mouse.move(move_x, move_y);
}


bool isPointerOffCentre(float difference_x, float difference_y) {
  /*
   *  Check if pointer is within deadzone but close to the edge
   */

  uint8_t dz_rad = registers.stick_deadzone_radius;
  uint8_t dz_centre = dz_rad / 2;

  // Exit early if it's already outside the deadzone
  // Don't want to trigger while you're moving the pointer
  if (difference_x < -dz_rad || difference_x > dz_rad ||
      difference_y < -dz_rad || difference_y > dz_rad) {
    return false;
  }

  // Check if X axis is on outside of deadzone
  if (difference_x > -dz_rad && difference_x < dz_rad) { // It's inside the deadzone
    if (difference_x < -dz_centre || difference_x > dz_centre) { // It's outside the middle of the deadzone
      // Therefore it's off-centre (by an amount we care about)
      return true;
    }
  }
  // Now check if Y axis is on outside of deadzone
  if (difference_y > -dz_rad && difference_y < dz_rad) { // It's inside the deadzone
    if (difference_y < -dz_centre || difference_y > dz_centre) { // It's outside the middle of the deadzone
      // Therefore it's off-centre (by an amount we care about)
      return true;
    }
  }

  // Otherwise it's either centred or outside the deadzone
  return false;
}


void updateLEDState() {
  /*
   *  Update the LED pin state based on the LED registers
   */

  digitalWrite(LED_USER1, registers.led_1);
  digitalWrite(LED_USER2, registers.led_2);
}


void setLED(uint8_t pin, bool state) {
  /*
   *  Set an LED on or off (and save the state to its register)
   *  pin == LED_USER1 or LED_USER2
   *  state == LED_ON or LED_OFF
   */

  if (pin == LED_USER1) {
    digitalWrite(LED_USER1, state);
    registers.led_1 = state;
  }
  else if (pin == LED_USER2) {
    digitalWrite(LED_USER2, state);
    registers.led_2 = state;
  }
}


void triggerStickRead() {
  registers.stick_trigger_read = true;
}
