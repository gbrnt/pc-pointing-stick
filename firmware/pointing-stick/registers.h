/*
 *  Registers for pointing stick variables
 *  Some registers are settings, some are outputs
 */

#include <stdbool.h>


enum operation_modes {
  PS_MODE_USB        = 0x00,
  PS_MODE_I2C_AUTO   = 0x01,
  PS_MODE_I2C_MANUAL = 0x02
};


struct Registers {
  uint8_t operation_mode;
  //bool run_pause;
  //bool stick_recentre;
  uint8_t stick_deadzone_radius;
  uint8_t stick_off_centre_count;
  uint8_t stick_recentre_threshold;
  bool stick_invert_x;
  bool stick_invert_y;
  uint16_t stick_baseline_x;
  uint16_t stick_baseline_y;
  bool stick_trigger_read;
  uint8_t stick_speed;
  uint8_t stick_readings_averaged;
  uint16_t stick_read_frequency;
  uint16_t stick_raw_x;
  uint16_t stick_raw_y;
  uint16_t stick_scaled_x;
  uint16_t stick_scaled_y;
  //bool pot_auto_centre_en;
  //bool pot_recentre;
  uint8_t pot_centre_x;
  uint8_t pot_centre_y;
  bool led_1;
  bool led_2;
};


// Register to store everything in one place
// This is to make it easy to access over i2c
struct Registers registers;


void initRegisters() {
  /*
   *  Set up registers with initial state
   */

  // Defaults to USB mode because pin is pulled up
  if (digitalRead(PIN_MODE) == HIGH) registers.operation_mode = PS_MODE_USB;
  else registers.operation_mode = PS_MODE_I2C_AUTO;

  // Stick settings
  registers.stick_deadzone_radius = 10;
  registers.stick_invert_x = 1;
  registers.stick_invert_y = 0;
  registers.stick_speed = 80;

  // How many ADC readings to average
  // More means less jitter in movement but lower maximum refresh rate
  // 10 -> ~4 ms to measure X and Y -> ~250Hz max refresh rate
  // 25 -> ~8 ms to measure X and Y -> ~125Hz max refresh rate
  registers.stick_readings_averaged = 10;

  // How often to read the ADC
  registers.stick_read_frequency = 100;

  // How many off-centre readings before it recentres
  registers.stick_recentre_threshold = 15;

  // Stick variables
  registers.stick_baseline_x = 0;
  registers.stick_baseline_y = 0;

  // Potentiometer variables
  registers.pot_centre_x = 127;
  registers.pot_centre_y = 127;
}

