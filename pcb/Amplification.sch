EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5750 2400 0    50   Input ~ 0
V_STRAIN_X
Text HLabel 5750 5400 0    50   Input ~ 0
V_STRAIN_Y
Text HLabel 5750 2600 0    50   Input ~ 0
V_REF_X
Text HLabel 5750 5600 0    50   Input ~ 0
V_REF_Y
Text HLabel 8750 2500 2    50   Input ~ 0
V_OUT_X
Text HLabel 8750 5500 2    50   Input ~ 0
V_OUT_Y
$Comp
L Amplifier_Operational:LM358 U4
U 1 1 604AF4BD
P 7000 2500
F 0 "U4" H 7000 2133 50  0000 C CNN
F 1 "LM358" H 7000 2224 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 7000 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 7000 2500 50  0001 C CNN
	1    7000 2500
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM358 U4
U 2 1 604B108F
P 7000 5500
F 0 "U4" H 7000 5133 50  0000 C CNN
F 1 "LM358" H 7000 5224 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 7000 5500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 7000 5500 50  0001 C CNN
	2    7000 5500
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM358 U4
U 3 1 604B27D8
P 2600 3750
F 0 "U4" H 2558 3796 50  0000 L CNN
F 1 "LM358" H 2558 3705 50  0000 L CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 2600 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2600 3750 50  0001 C CNN
	3    2600 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR016
U 1 1 604C3AB7
P 2500 3250
F 0 "#PWR016" H 2500 3100 50  0001 C CNN
F 1 "VDD" H 2515 3423 50  0000 C CNN
F 2 "" H 2500 3250 50  0001 C CNN
F 3 "" H 2500 3250 50  0001 C CNN
	1    2500 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 604C3F5E
P 2500 4250
F 0 "#PWR017" H 2500 4000 50  0001 C CNN
F 1 "GND" H 2505 4077 50  0000 C CNN
F 2 "" H 2500 4250 50  0001 C CNN
F 3 "" H 2500 4250 50  0001 C CNN
	1    2500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3450 2500 3250
Wire Wire Line
	2500 4050 2500 4250
$Comp
L Device:C C12
U 1 1 604C77B0
P 6250 2750
F 0 "C12" H 6365 2796 50  0000 L CNN
F 1 "0.1uF" H 6365 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6288 2600 50  0001 C CNN
F 3 "~" H 6250 2750 50  0001 C CNN
	1    6250 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 604C83E7
P 6250 5750
F 0 "C13" H 6365 5796 50  0000 L CNN
F 1 "0.1uF" H 6365 5705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6288 5600 50  0001 C CNN
F 3 "~" H 6250 5750 50  0001 C CNN
	1    6250 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2600 6250 2600
Connection ~ 6250 2600
Wire Wire Line
	6250 2600 5750 2600
Wire Wire Line
	6700 5600 6250 5600
Connection ~ 6250 5600
Wire Wire Line
	6250 5600 5750 5600
$Comp
L power:GND #PWR019
U 1 1 604C926A
P 6250 6000
F 0 "#PWR019" H 6250 5750 50  0001 C CNN
F 1 "GND" H 6255 5827 50  0000 C CNN
F 2 "" H 6250 6000 50  0001 C CNN
F 3 "" H 6250 6000 50  0001 C CNN
	1    6250 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 604C965D
P 6250 3000
F 0 "#PWR018" H 6250 2750 50  0001 C CNN
F 1 "GND" H 6255 2827 50  0000 C CNN
F 2 "" H 6250 3000 50  0001 C CNN
F 3 "" H 6250 3000 50  0001 C CNN
	1    6250 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2900 6250 3000
Wire Wire Line
	6250 5900 6250 6000
$Comp
L Device:R R14
U 1 1 604CA614
P 6250 2400
F 0 "R14" V 6043 2400 50  0000 C CNN
F 1 "10K" V 6134 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6180 2400 50  0001 C CNN
F 3 "~" H 6250 2400 50  0001 C CNN
	1    6250 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 604CAF64
P 6250 5400
F 0 "R15" V 6043 5400 50  0000 C CNN
F 1 "10K" V 6134 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6180 5400 50  0001 C CNN
F 3 "~" H 6250 5400 50  0001 C CNN
	1    6250 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 5400 6100 5400
Wire Wire Line
	6400 5400 6550 5400
Wire Wire Line
	6700 2400 6550 2400
Wire Wire Line
	6100 2400 5750 2400
$Comp
L Device:R R16
U 1 1 604CCC0C
P 7000 1500
F 0 "R16" V 6793 1500 50  0000 C CNN
F 1 "1M" V 6884 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6930 1500 50  0001 C CNN
F 3 "~" H 7000 1500 50  0001 C CNN
	1    7000 1500
	0    1    1    0   
$EndComp
$Comp
L Device:R R20
U 1 1 604CDBCF
P 7500 1500
F 0 "R20" V 7293 1500 50  0000 C CNN
F 1 "470K" V 7384 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7430 1500 50  0001 C CNN
F 3 "~" H 7500 1500 50  0001 C CNN
	1    7500 1500
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 604CE1CF
P 8000 1500
F 0 "R24" V 7793 1500 50  0000 C CNN
F 1 "470K" V 7884 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7930 1500 50  0001 C CNN
F 3 "~" H 8000 1500 50  0001 C CNN
	1    8000 1500
	0    1    1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 604CEA8A
P 7250 1750
F 0 "R18" H 7320 1796 50  0000 L CNN
F 1 "0R" H 7320 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7180 1750 50  0001 C CNN
F 3 "~" H 7250 1750 50  0001 C CNN
	1    7250 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 604CF043
P 7750 1750
F 0 "R22" H 7820 1796 50  0000 L CNN
F 1 "0R" H 7820 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7680 1750 50  0001 C CNN
F 3 "~" H 7750 1750 50  0001 C CNN
	1    7750 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R26
U 1 1 604CF3F0
P 8250 1750
F 0 "R26" H 8320 1796 50  0000 L CNN
F 1 "0R" H 8320 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 8180 1750 50  0001 C CNN
F 3 "~" H 8250 1750 50  0001 C CNN
	1    8250 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2400 6550 1500
Wire Wire Line
	6550 1500 6850 1500
Connection ~ 6550 2400
Wire Wire Line
	6550 2400 6400 2400
Wire Wire Line
	7150 1500 7250 1500
Wire Wire Line
	7650 1500 7750 1500
Wire Wire Line
	8150 1500 8250 1500
Wire Wire Line
	8250 1500 8250 1600
Wire Wire Line
	7750 1500 7750 1600
Connection ~ 7750 1500
Wire Wire Line
	7750 1500 7850 1500
Wire Wire Line
	7250 1600 7250 1500
Connection ~ 7250 1500
Wire Wire Line
	7250 1500 7350 1500
Wire Wire Line
	7250 1900 7250 2000
Wire Wire Line
	7250 2000 7750 2000
Wire Wire Line
	8250 2000 8250 1900
Wire Wire Line
	7750 2500 7300 2500
Wire Wire Line
	7750 1900 7750 2000
Connection ~ 7750 2000
Wire Wire Line
	7750 2000 8250 2000
Wire Wire Line
	7750 2000 7750 2500
Wire Wire Line
	7750 2500 8750 2500
Connection ~ 7750 2500
$Comp
L Device:R R17
U 1 1 604DA2DE
P 7000 4500
F 0 "R17" V 6793 4500 50  0000 C CNN
F 1 "1M" V 6884 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6930 4500 50  0001 C CNN
F 3 "~" H 7000 4500 50  0001 C CNN
	1    7000 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R21
U 1 1 604DA2E4
P 7500 4500
F 0 "R21" V 7293 4500 50  0000 C CNN
F 1 "470K" V 7384 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7430 4500 50  0001 C CNN
F 3 "~" H 7500 4500 50  0001 C CNN
	1    7500 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 604DA2EA
P 8000 4500
F 0 "R25" V 7793 4500 50  0000 C CNN
F 1 "470K" V 7884 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7930 4500 50  0001 C CNN
F 3 "~" H 8000 4500 50  0001 C CNN
	1    8000 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R19
U 1 1 604DA2F0
P 7250 4750
F 0 "R19" H 7320 4796 50  0000 L CNN
F 1 "0R" H 7320 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7180 4750 50  0001 C CNN
F 3 "~" H 7250 4750 50  0001 C CNN
	1    7250 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R23
U 1 1 604DA2F6
P 7750 4750
F 0 "R23" H 7820 4796 50  0000 L CNN
F 1 "0R" H 7820 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7680 4750 50  0001 C CNN
F 3 "~" H 7750 4750 50  0001 C CNN
	1    7750 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R27
U 1 1 604DA2FC
P 8250 4750
F 0 "R27" H 8320 4796 50  0000 L CNN
F 1 "0R" H 8320 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 8180 4750 50  0001 C CNN
F 3 "~" H 8250 4750 50  0001 C CNN
	1    8250 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 4500 6850 4500
Wire Wire Line
	7150 4500 7250 4500
Wire Wire Line
	7650 4500 7750 4500
Wire Wire Line
	8150 4500 8250 4500
Wire Wire Line
	8250 4500 8250 4600
Wire Wire Line
	7750 4500 7750 4600
Connection ~ 7750 4500
Wire Wire Line
	7750 4500 7850 4500
Wire Wire Line
	7250 4600 7250 4500
Connection ~ 7250 4500
Wire Wire Line
	7250 4500 7350 4500
Wire Wire Line
	7250 4900 7250 5000
Wire Wire Line
	7250 5000 7750 5000
Wire Wire Line
	8250 5000 8250 4900
Wire Wire Line
	7750 4900 7750 5000
Connection ~ 7750 5000
Wire Wire Line
	7750 5000 8250 5000
Wire Wire Line
	7750 5000 7750 5500
Wire Wire Line
	6550 4500 6550 5400
Connection ~ 6550 5400
Wire Wire Line
	6550 5400 6700 5400
Wire Wire Line
	7300 5500 7750 5500
Connection ~ 7750 5500
Wire Wire Line
	7750 5500 8750 5500
Wire Notes Line
	5000 1000 9500 1000
Wire Notes Line
	9500 1000 9500 3500
Wire Notes Line
	9500 3500 5000 3500
Wire Notes Line
	5000 3500 5000 1000
Wire Notes Line
	5000 4000 9500 4000
Wire Notes Line
	9500 4000 9500 6500
Wire Notes Line
	9500 6500 5000 6500
Wire Notes Line
	5000 6500 5000 4000
Text Notes 5000 950  0    50   ~ 0
X-axis amplification\n100-200x gain depending on where 0R jumper is\nAmplifies difference between strain gauge and reference
Text Notes 5000 3950 0    50   ~ 0
Y-axis amplification\n100-200x gain depending on where 0R jumper is\nAmplifies difference between strain gauge and reference
Wire Notes Line
	7350 1200 7350 1600
Wire Notes Line
	7350 1600 7600 1600
Wire Notes Line
	7600 1600 7600 1900
Wire Notes Line
	7600 1900 8500 1900
Wire Notes Line
	8500 1900 8500 1200
Wire Notes Line
	8500 1200 7350 1200
Text Notes 7350 1150 0    50   ~ 0
DNP by default, add to increase gain
Text Notes 7350 4150 0    50   ~ 0
DNP by default, add to increase gain
Wire Notes Line
	7350 4600 7350 4200
Wire Notes Line
	7350 4200 8500 4200
Wire Notes Line
	8500 4200 8500 4900
Wire Notes Line
	8500 4900 7550 4900
Wire Notes Line
	7550 4900 7550 4600
Wire Notes Line
	7550 4600 7350 4600
$Comp
L Device:C C11
U 1 1 604F535A
P 3000 3750
F 0 "C11" H 3115 3796 50  0000 L CNN
F 1 "0.1uF" H 3115 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3038 3600 50  0001 C CNN
F 3 "~" H 3000 3750 50  0001 C CNN
	1    3000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3450 3000 3450
Wire Wire Line
	3000 3450 3000 3600
Connection ~ 2500 3450
Wire Wire Line
	3000 3900 3000 4050
Wire Wire Line
	3000 4050 2500 4050
Connection ~ 2500 4050
Wire Notes Line
	2000 2750 3500 2750
Wire Notes Line
	3500 2750 3500 4750
Wire Notes Line
	3500 4750 2000 4750
Wire Notes Line
	2000 4750 2000 2750
Text Notes 2000 2700 0    50   ~ 0
Power + decoupling\nfor LM358 dual op-amp
$EndSCHEMATC
