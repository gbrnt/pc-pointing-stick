EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2450 2250 0    50   ~ 0
VIN_RAW
Wire Wire Line
	1700 2350 1500 2350
Wire Wire Line
	1500 2450 1700 2450
$Comp
L power:GND #PWR0101
U 1 1 603AB673
P 1750 2800
F 0 "#PWR0101" H 1750 2550 50  0001 C CNN
F 1 "GND" H 1755 2627 50  0000 C CNN
F 2 "" H 1750 2800 50  0001 C CNN
F 3 "" H 1750 2800 50  0001 C CNN
	1    1750 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2550 1750 2550
Wire Wire Line
	1750 2550 1750 2800
$Sheet
S 5500 2000 1500 1250
U 6035DC2E
F0 "Strain gauges" 50
F1 "Strain.sch" 50
F2 "V_EXCITE" I L 5500 2250 50 
F3 "V_STRAIN_X" I R 7000 2250 50 
F4 "V_STRAIN_Y" I R 7000 2500 50 
F5 "V_REF_X" I R 7000 2750 50 
F6 "V_REF_Y" I R 7000 3000 50 
F7 "POT_SHDN" I L 5500 3000 50 
F8 "POT_SDO" I L 5500 2900 50 
F9 "POT_SDI" I L 5500 2800 50 
F10 "POT_SCK" I L 5500 2700 50 
F11 "POT_CS" I L 5500 2600 50 
$EndSheet
$Sheet
S 3000 2000 1500 1250
U 6035C21A
F0 "Power" 50
F1 "Power.sch" 50
F2 "VIN_RAW" I L 3000 2250 50 
F3 "V_EXCITE" I R 4500 3000 50 
$EndSheet
Wire Wire Line
	4500 3000 4750 3000
Wire Wire Line
	7000 2500 8500 2500
Wire Wire Line
	7000 2750 8500 2750
Wire Wire Line
	7000 3000 8500 3000
Text Label 10150 2250 0    50   ~ 0
V_AMPLIFIED_X
Text Label 10150 2750 0    50   ~ 0
V_AMPLIFIED_Y
Wire Notes Line
	1000 1750 2250 1750
Wire Notes Line
	2250 1750 2250 3250
Wire Notes Line
	2250 3250 1000 3250
Wire Notes Line
	1000 3250 1000 1750
Text Notes 1000 1700 0    50   ~ 0
Connection to use amp\nwith external microcontroller.\n(U3 will still need to set reference voltages)\nVIN_RAW should be 3.3-5.5V
$Sheet
S 3000 4000 1500 1500
U 603854DE
F0 "Microcontroller" 50
F1 "Microcontroller.sch" 50
F2 "VIN_X" I L 3000 4500 50 
F3 "VIN_Y" I L 3000 5000 50 
F4 "SDA" I R 4500 5150 50 
F5 "SCL" I R 4500 5250 50 
F6 "POT_SHDN" I R 4500 4650 50 
F7 "POT_SDO" I R 4500 4550 50 
F8 "POT_SDI" I R 4500 4450 50 
F9 "POT_SCK" I R 4500 4350 50 
F10 "POT_CS" I R 4500 4250 50 
F11 "V_EXCITE" I L 3000 4250 50 
F12 "INT_OUT" I R 4500 5050 50 
$EndSheet
Text Notes 1000 3450 0    50   ~ 0
Do not connect VIN_RAW here\nand USB at the same time
Wire Wire Line
	10000 2250 10150 2250
Wire Wire Line
	10150 2750 10000 2750
$Sheet
S 8500 2000 1500 1250
U 603BA0C4
F0 "Amplification" 50
F1 "Amplification.sch" 50
F2 "V_STRAIN_X" I L 8500 2250 50 
F3 "V_STRAIN_Y" I L 8500 2500 50 
F4 "V_REF_X" I L 8500 2750 50 
F5 "V_REF_Y" I L 8500 3000 50 
F6 "V_OUT_X" I R 10000 2250 50 
F7 "V_OUT_Y" I R 10000 2750 50 
$EndSheet
Wire Wire Line
	1500 2250 3000 2250
Wire Wire Line
	5500 2250 4750 2250
Wire Wire Line
	4750 2250 4750 3000
Text Notes 3000 1850 0    50   ~ 0
Reservoir capacitors and\ngeneration of V_EXCITE (2.5V)
Wire Wire Line
	7000 2250 8500 2250
Text Notes 5500 1850 0    50   ~ 0
X and Y axes\nand reference voltages
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 6035FA81
P 1300 2450
F 0 "J2" H 1218 2025 50  0000 C CNN
F 1 "RAW OUTPUT" H 1218 2116 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1300 2450 50  0001 C CNN
F 3 "~" H 1300 2450 50  0001 C CNN
	1    1300 2450
	-1   0    0    1   
$EndComp
Text Label 2250 4500 0    50   ~ 0
V_AMPLIFIED_X
Text Label 2250 5000 0    50   ~ 0
V_AMPLIFIED_Y
Wire Wire Line
	3000 4500 2250 4500
Wire Wire Line
	2250 5000 3000 5000
Wire Wire Line
	5500 2600 4850 2600
Wire Wire Line
	4850 2600 4850 4250
Wire Wire Line
	4850 4250 4500 4250
Wire Wire Line
	4500 4350 4950 4350
Wire Wire Line
	4950 4350 4950 2700
Wire Wire Line
	4950 2700 5500 2700
Wire Wire Line
	5500 2800 5050 2800
Wire Wire Line
	5050 2800 5050 4450
Wire Wire Line
	5050 4450 4500 4450
Wire Wire Line
	4500 4550 5150 4550
Wire Wire Line
	5150 4550 5150 2900
Wire Wire Line
	5150 2900 5500 2900
Wire Wire Line
	5250 4650 4500 4650
Wire Wire Line
	5250 3000 5500 3000
Wire Wire Line
	5250 3000 5250 4650
Wire Wire Line
	4750 3000 4750 3500
Wire Wire Line
	4750 3500 2750 3500
Wire Wire Line
	2750 3500 2750 4250
Wire Wire Line
	2750 4250 3000 4250
Connection ~ 4750 3000
Text Label 1700 2450 0    50   ~ 0
V_AMPLIFIED_X
Text Label 1700 2350 0    50   ~ 0
V_AMPLIFIED_Y
$Comp
L Connector_Generic:Conn_01x05 J3
U 1 1 6049CAC5
P 6700 5150
F 0 "J3" H 6618 4725 50  0000 C CNN
F 1 "I2C OUTPUT" H 6618 4816 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6700 5150 50  0001 C CNN
F 3 "~" H 6700 5150 50  0001 C CNN
	1    6700 5150
	1    0    0    1   
$EndComp
Wire Wire Line
	4500 5050 6500 5050
Wire Wire Line
	6500 5150 4500 5150
Wire Wire Line
	4500 5250 6500 5250
$Comp
L power:GND #PWR01
U 1 1 604A127C
P 6250 5500
F 0 "#PWR01" H 6250 5250 50  0001 C CNN
F 1 "GND" H 6255 5327 50  0000 C CNN
F 2 "" H 6250 5500 50  0001 C CNN
F 3 "" H 6250 5500 50  0001 C CNN
	1    6250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5350 6250 5350
Wire Wire Line
	6250 5350 6250 5500
Text Label 6000 4950 0    50   ~ 0
VIN_RAW
Wire Wire Line
	6000 4950 6500 4950
Wire Notes Line
	5750 4500 7000 4500
Wire Notes Line
	7000 4500 7000 6000
Wire Notes Line
	7000 6000 5750 6000
Wire Notes Line
	5750 6000 5750 4500
Text Notes 5750 4450 0    50   ~ 0
Connector to use as i2c peripheral\nto external microcontroller
$EndSCHEMATC
