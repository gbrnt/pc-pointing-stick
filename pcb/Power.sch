EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Power"
Date ""
Rev ""
Comp "George Bryant"
Comment1 "Making VDD and V_EXCITE from input voltage"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2200 3500 0    50   Input ~ 0
VIN_RAW
Text HLabel 9250 3500 2    50   Input ~ 0
V_EXCITE
$Comp
L Device:C C1
U 1 1 603688DC
P 3250 3750
F 0 "C1" H 3365 3796 50  0000 L CNN
F 1 "10uF" H 3365 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3288 3600 50  0001 C CNN
F 3 "~" H 3250 3750 50  0001 C CNN
	1    3250 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 6036AFF9
P 3750 3750
F 0 "C2" H 3865 3796 50  0000 L CNN
F 1 "1uF" H 3865 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3788 3600 50  0001 C CNN
F 3 "~" H 3750 3750 50  0001 C CNN
	1    3750 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 6036B585
P 4250 3750
F 0 "C3" H 4365 3796 50  0000 L CNN
F 1 "100nF" H 4365 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 4288 3600 50  0001 C CNN
F 3 "~" H 4250 3750 50  0001 C CNN
	1    4250 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 6036B840
P 4750 3750
F 0 "C4" H 4865 3796 50  0000 L CNN
F 1 "10nF" H 4865 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 4788 3600 50  0001 C CNN
F 3 "~" H 4750 3750 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3500 4750 3600
Wire Wire Line
	4250 3600 4250 3500
Connection ~ 4250 3500
Wire Wire Line
	4250 3500 4750 3500
Wire Wire Line
	2750 3500 3250 3500
Wire Wire Line
	3750 3500 3750 3600
Connection ~ 3750 3500
Wire Wire Line
	3250 3500 3250 3600
Connection ~ 3250 3500
Wire Wire Line
	3250 3500 3750 3500
Wire Wire Line
	2450 3500 2200 3500
Connection ~ 4750 3500
Wire Wire Line
	5200 4000 4750 4000
Wire Wire Line
	3250 4000 3250 3900
Wire Wire Line
	3750 3900 3750 4000
Connection ~ 3750 4000
Wire Wire Line
	3750 4000 3250 4000
Wire Wire Line
	4250 3900 4250 4000
Connection ~ 4250 4000
Wire Wire Line
	4250 4000 3750 4000
Wire Wire Line
	4750 3900 4750 4000
Connection ~ 4750 4000
Wire Wire Line
	4750 4000 4250 4000
Wire Wire Line
	3750 3500 4250 3500
$Comp
L Reference_Voltage:TL431DBZ U2
U 1 1 6037C2F4
P 8250 4000
F 0 "U2" V 8296 3931 50  0000 R CNN
F 1 "TL431DBZ" V 8205 3931 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8250 3850 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tl431.pdf" H 8250 4000 50  0001 C CIN
	1    8250 4000
	0    1    -1   0   
$EndComp
Wire Wire Line
	8250 3500 8250 3900
$Comp
L Device:R R5
U 1 1 60395DD8
P 7500 3500
F 0 "R5" V 7707 3500 50  0000 C CNN
F 1 "330R" V 7616 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7430 3500 50  0001 C CNN
F 3 "~" H 7500 3500 50  0001 C CNN
	1    7500 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7650 3500 8250 3500
Wire Wire Line
	8250 3500 8750 3500
Connection ~ 8250 3500
Wire Wire Line
	8350 4000 8750 4000
Wire Wire Line
	8750 4000 8750 3500
Connection ~ 8750 3500
Wire Wire Line
	8750 3500 9250 3500
Wire Wire Line
	8250 4100 8250 4400
Wire Notes Line
	7000 3000 10000 3000
Wire Notes Line
	10000 3000 10000 5000
Wire Notes Line
	10000 5000 7000 5000
Wire Notes Line
	7000 5000 7000 3000
Wire Notes Line
	1500 2750 6750 2750
Wire Notes Line
	6750 2750 6750 4500
Wire Notes Line
	6750 4500 1500 4500
Wire Notes Line
	1500 4500 1500 2750
Text Notes 1500 2700 0    50   ~ 0
Smoothing capacitors for power input\nVIN_RAW should be 5V\n3.3 might work but gain may need adjustment
Text Notes 7000 2950 0    50   ~ 0
Create V_EXCITE (2.5V) from VDD\nUsing TL431 in fixed zener mode
$Comp
L power:GND #PWR0108
U 1 1 603AD824
P 8250 4500
F 0 "#PWR0108" H 8250 4250 50  0001 C CNN
F 1 "GND" H 8255 4327 50  0000 C CNN
F 2 "" H 8250 4500 50  0001 C CNN
F 3 "" H 8250 4500 50  0001 C CNN
	1    8250 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 603AD96A
P 5200 4000
F 0 "#PWR0109" H 5200 3750 50  0001 C CNN
F 1 "GND" H 5205 3827 50  0000 C CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "" H 5200 4000 50  0001 C CNN
	1    5200 4000
	1    0    0    -1  
$EndComp
Text Notes 8200 4200 2    50   ~ 0
SOT-23-3 version
Text Notes 3250 3400 0    50   ~ 0
All ceramic to keep size small
$Comp
L Device:D_Schottky D1
U 1 1 603B5B1A
P 2600 3500
F 0 "D1" H 2600 3283 50  0000 C CNN
F 1 "D_Schottky" H 2600 3374 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 2600 3500 50  0001 C CNN
F 3 "~" H 2600 3500 50  0001 C CNN
	1    2600 3500
	-1   0    0    1   
$EndComp
$Comp
L power:VDD #PWR0110
U 1 1 6039D9ED
P 6000 3500
F 0 "#PWR0110" H 6000 3350 50  0001 C CNN
F 1 "VDD" H 6015 3673 50  0000 C CNN
F 2 "" H 6000 3500 50  0001 C CNN
F 3 "" H 6000 3500 50  0001 C CNN
	1    6000 3500
	1    0    0    -1  
$EndComp
Connection ~ 6000 3500
Wire Wire Line
	6000 3500 7350 3500
$Comp
L Device:C C5
U 1 1 60562A43
P 8750 4150
F 0 "C5" H 8865 4196 50  0000 L CNN
F 1 "100nF" H 8865 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 8788 4000 50  0001 C CNN
F 3 "~" H 8750 4150 50  0001 C CNN
	1    8750 4150
	1    0    0    -1  
$EndComp
Connection ~ 8750 4000
Wire Wire Line
	8250 4400 8750 4400
Wire Wire Line
	8750 4400 8750 4300
Connection ~ 8250 4400
Wire Wire Line
	8250 4400 8250 4500
Wire Wire Line
	4750 3500 6000 3500
$EndSCHEMATC
