EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2500 4000 2    50   Input ~ 0
V_STRAIN_X
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 603D77C9
P 2300 3600
F 0 "J5" H 2380 3592 50  0000 L CNN
F 1 "GAUGE_X1" H 2380 3501 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 2300 3600 50  0001 C CNN
F 3 "~" H 2300 3600 50  0001 C CNN
	1    2300 3600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 603D8F54
P 2300 4500
F 0 "J6" H 2380 4492 50  0000 L CNN
F 1 "GAUGE_X2" H 2380 4401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 2300 4500 50  0001 C CNN
F 3 "~" H 2300 4500 50  0001 C CNN
	1    2300 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 4400 2500 3600
Text HLabel 2500 3000 1    50   Input ~ 0
V_EXCITE
$Comp
L power:GND #PWR0102
U 1 1 603DCE6F
P 2500 5000
F 0 "#PWR0102" H 2500 4750 50  0001 C CNN
F 1 "GND" H 2505 4827 50  0000 C CNN
F 2 "" H 2500 5000 50  0001 C CNN
F 3 "" H 2500 5000 50  0001 C CNN
	1    2500 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4500 2500 5000
Wire Wire Line
	2500 3500 2500 3000
Text HLabel 4000 4000 2    50   Input ~ 0
V_STRAIN_Y
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 603E192B
P 3800 3600
F 0 "J7" H 3880 3592 50  0000 L CNN
F 1 "GAUGE_Y1" H 3880 3501 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 3800 3600 50  0001 C CNN
F 3 "~" H 3800 3600 50  0001 C CNN
	1    3800 3600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 603E1931
P 3800 4500
F 0 "J8" H 3880 4492 50  0000 L CNN
F 1 "GAUGE_Y2" H 3880 4401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 3800 4500 50  0001 C CNN
F 3 "~" H 3800 4500 50  0001 C CNN
	1    3800 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 4400 4000 3600
Text HLabel 4000 3000 1    50   Input ~ 0
V_EXCITE
$Comp
L power:GND #PWR0103
U 1 1 603E1939
P 4000 5000
F 0 "#PWR0103" H 4000 4750 50  0001 C CNN
F 1 "GND" H 4005 4827 50  0000 C CNN
F 2 "" H 4000 5000 50  0001 C CNN
F 3 "" H 4000 5000 50  0001 C CNN
	1    4000 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4500 4000 5000
Wire Wire Line
	4000 3500 4000 3000
Text HLabel 10000 3300 1    50   Input ~ 0
V_EXCITE
Text Notes 6500 2450 0    50   ~ 0
Use digital potentiometer to generate reference voltages for strain gauges\nChannel 0 is X axis, channel 1 is Y axis\nWith these resistors, V_REF_(X|Y) is between 1.128 and 1.368 V
Wire Notes Line
	6500 5500 6500 2500
Wire Notes Line
	10500 5500 6500 5500
Wire Notes Line
	10500 2500 10500 5500
Wire Notes Line
	6500 2500 10500 2500
Wire Wire Line
	10250 4400 10250 4650
Connection ~ 10250 4400
Wire Wire Line
	9800 4400 10250 4400
Wire Wire Line
	10250 3800 10250 4400
Wire Wire Line
	9800 3800 10250 3800
$Comp
L power:GND #PWR0104
U 1 1 603BFF56
P 10250 4650
F 0 "#PWR0104" H 10250 4400 50  0001 C CNN
F 1 "GND" H 10255 4477 50  0000 C CNN
F 2 "" H 10250 4650 50  0001 C CNN
F 3 "" H 10250 4650 50  0001 C CNN
	1    10250 4650
	1    0    0    -1  
$EndComp
Connection ~ 10000 3500
Wire Wire Line
	10000 4100 10000 3500
Wire Wire Line
	9800 4100 10000 4100
Wire Wire Line
	10000 3500 10000 3300
Wire Wire Line
	9800 3500 10000 3500
$Comp
L Potentiometer_Digital:MCP4251-xxxx-ST U1
U 1 1 603BB3D7
P 8000 3950
F 0 "U1" H 8100 3350 50  0000 L CNN
F 1 "MCP4251-104E/ST" H 8100 3250 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 8000 2950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/22060b.pdf" H 8200 3800 50  0001 C CNN
	1    8000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 3650 9500 3500
Wire Wire Line
	8400 3650 9500 3650
Wire Wire Line
	9500 4250 9500 4400
Wire Wire Line
	8400 4250 9500 4250
Wire Wire Line
	9500 4050 9500 4100
Wire Wire Line
	8400 4050 9500 4050
Wire Wire Line
	9500 3850 9500 3800
Wire Wire Line
	8400 3850 9500 3850
$Comp
L Device:R R4
U 1 1 603B649E
P 9650 4400
F 0 "R4" V 9857 4400 50  0000 C CNN
F 1 "470k" V 9766 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 9580 4400 50  0001 C CNN
F 3 "~" H 9650 4400 50  0001 C CNN
	1    9650 4400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 603B401D
P 9650 4100
F 0 "R3" V 9857 4100 50  0000 C CNN
F 1 "470k" V 9766 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 9580 4100 50  0001 C CNN
F 3 "~" H 9650 4100 50  0001 C CNN
	1    9650 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 603B28BF
P 9650 3800
F 0 "R2" V 9857 3800 50  0000 C CNN
F 1 "470k" V 9766 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 9580 3800 50  0001 C CNN
F 3 "~" H 9650 3800 50  0001 C CNN
	1    9650 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 603AAA11
P 9650 3500
F 0 "R1" V 9443 3500 50  0000 C CNN
F 1 "470k" V 9534 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 9580 3500 50  0001 C CNN
F 3 "~" H 9650 3500 50  0001 C CNN
	1    9650 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 4250 6750 4250
$Comp
L power:VDD #PWR0105
U 1 1 603A8478
P 6750 4250
F 0 "#PWR0105" H 6750 4100 50  0001 C CNN
F 1 "VDD" H 6765 4423 50  0000 C CNN
F 2 "" H 6750 4250 50  0001 C CNN
F 3 "" H 6750 4250 50  0001 C CNN
	1    6750 4250
	1    0    0    -1  
$EndComp
Text Notes 8100 4800 0    50   ~ 0
100k resistance TSSOP variant
Text HLabel 7600 4150 0    50   Input ~ 0
POT_SHDN
$Comp
L power:VDD #PWR0106
U 1 1 603A3235
P 8000 2900
F 0 "#PWR0106" H 8000 2750 50  0001 C CNN
F 1 "VDD" H 8015 3073 50  0000 C CNN
F 2 "" H 8000 2900 50  0001 C CNN
F 3 "" H 8000 2900 50  0001 C CNN
	1    8000 2900
	1    0    0    -1  
$EndComp
Text HLabel 7600 3950 0    50   Input ~ 0
POT_SDO
Text HLabel 7600 3850 0    50   Input ~ 0
POT_SDI
Text HLabel 7600 3750 0    50   Input ~ 0
POT_SCK
Text HLabel 7600 3650 0    50   Input ~ 0
POT_CS
Wire Wire Line
	8000 2900 8000 3350
Wire Wire Line
	8000 4550 8000 4900
$Comp
L power:GND #PWR0107
U 1 1 603CFFC2
P 8000 4900
F 0 "#PWR0107" H 8000 4650 50  0001 C CNN
F 1 "GND" H 8005 4727 50  0000 C CNN
F 2 "" H 8000 4900 50  0001 C CNN
F 3 "" H 8000 4900 50  0001 C CNN
	1    8000 4900
	1    0    0    -1  
$EndComp
Text HLabel 8400 4150 2    50   Input ~ 0
V_REF_Y
Text HLabel 8400 3750 2    50   Input ~ 0
V_REF_X
Wire Notes Line
	1500 2500 4750 2500
Wire Notes Line
	4750 2500 4750 5500
Wire Notes Line
	4750 5500 1500 5500
Wire Notes Line
	1500 5500 1500 2500
Text Notes 1500 2450 0    50   ~ 0
Strain gauge voltage dividers for X and Y axes\nWhen GAUGE_X1's resistance increases, GAUGE_X2's decreases\nThis doubles the V_STRAIN_X change compared to using just one gauge
$EndSCHEMATC
