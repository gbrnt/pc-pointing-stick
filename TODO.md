# TODO
### Documentation

- Summary of working principle
- Summary of electronics
- Summary of mechanics
- Guide to set up as an i2c peripheral
- Peripheral command reference/list

## Features
### Software

- More useful LED feedback
- USB/i2c mode select with jumper
- Automatic drift correction algorithm
- X/Y swap register

### Hardware

- Test points for VREF, VIN and VOUT on X and Y
- Smaller (flatter) crystal
- Less capacitance is probably required
- Some sort of ESD protection?
- GND via stitching around outside
- Remove extra gain resistors (once a reasonable gain is chosen)
- Larger 5V traces
- Less messy routing
- Use actual PCB as beams for strain gauges
- Use smaller microcontroller (ATMega32u4 is excessive)
- Add indication of which state is which for mode select jumper
- Build into keyboard properly as daughterboard

## Issues
### Software

### Hardware

- i2c pullups are missing
- VDD pin on ICSP header
- 
